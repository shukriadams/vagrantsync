#!/usr/bin/env node

var chokidar = require('chokidar'),
    fs = require('fs-extra'),
    path = require('path'),
    targetRoot = '/home/vagrant';

// One-liner for current directory, ignores .dotfiles
chokidar.watch('/vagrant', { ignored: [/node_modules/, /.idea/, /.git/], usePolling : true, ignoreInitial: false } ).on('all',  function (event, filepath){
    console.log(event, filepath);

    var targetAbsPath = path.join(targetRoot, filepath);

    try {
        if (event === 'unlink')
            fs.unlinkSync(targetAbsPath);
        else
            fs.copySync(filepath, targetAbsPath);
    }catch(ex){
        console.log(ex);
    }

});

console.log('watching');

